<?php

/**
 * @file
 * PMB reader account suggestions template.
 */

$header = array(
  t('Title'),
  t('Author'),
  t('Publisher'),
  t('State'),
  t('View/Edit'),
);
$rows = array();

if (isset($suggestions) && is_array($suggestions) && count($suggestions)) {
  foreach ($suggestions as $asuggestion) {
    $link = ($asuggestion->sugg_state == 1) ?
        l(t('View/Edit'), 'reader/' . $reader->uid . '/suggestion/' . $asuggestion->sugg_id) :
        l(t('View'), 'reader/' . $reader->uid . '/suggestion/' . $asuggestion->sugg_id);
    $rows[] = array(
      check_plain($asuggestion->sugg_title),
      check_plain($asuggestion->sugg_author),
      check_plain($asuggestion->sugg_editor),
      check_plain($asuggestion->sugg_state_caption),
      $link,
    );
  }
}

$template .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No suggestion.')));

$template .= '<br />';
$template .= l(t('Add a suggestion'), 'reader/' . $reader->uid . '/suggestion/add');
