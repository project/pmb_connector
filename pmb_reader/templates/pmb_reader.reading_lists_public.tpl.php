<?php

/**
 * @file
 * PMB reader public reading lists template.
 */

$header = array(
  t('Name'),
  t('Description'),
  t('Author'),
  t('Access'),
);
$rows = array();

if (isset($reading_lists) && is_array($reading_lists) && count($reading_lists)) {
  foreach ($reading_lists as $a_reading_list) {
    $link = l($a_reading_list->reading_list_name, 'reader/' . $reader->uid . '/reading_list_public/' . $a_reading_list->reading_list_id);
    $access = t('Open');
    if ($a_reading_list->reading_list_confidential && $a_reading_list->reading_list_public) {
      $access = t('Confidential');
    }
    $rows[] = array(
      $link,
      check_plain($a_reading_list->reading_list_caption),
      check_plain($a_reading_list->reading_list_empr_caption),
      $access,
    );
  }
}

$template .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No public reading list.')));
