<?php

/**
 * @file
 * PMB reader reading lists template.
 */

$header = array(
  t('Name'),
  t('Description'),
);
$rows = array();

if (isset($reading_lists) && is_array($reading_lists) && count($reading_lists)) {
  foreach ($reading_lists as $a_reading_list) {
    $link = l($a_reading_list->reading_list_name, 'reader/' . $reader->uid . '/reading_list/' . $a_reading_list->reading_list_id);
    $rows[] = array(
      $link,
      check_plain($a_reading_list->reading_list_caption),
    );
  }
}

$template .= '<h2>' . t('Your lists') . '</h2>';
$template .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No reading list.')));

$template .= '<h2>' . t('Public lists') . '</h2>';
$template .= l(t('View public reading lists'), 'reader/' . $reader->uid . '/reading_list_public/');
