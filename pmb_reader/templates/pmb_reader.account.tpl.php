<?php

/**
 * @file
 * PMB reader account summary template.
 */

$header = array();
$rows = array();

$rows[] = array(
  t('Last Name'),
  check_plain($reader->data['pmb_reader']['pmb_reader_info']->personal_information->lastname),
);
$rows[] = array(
  t('First Name'),
  check_plain($reader->data['pmb_reader']['pmb_reader_info']->personal_information->firstname),
);
$rows[] = array(
  t('Bar code'),
  check_plain($reader->data['pmb_reader']['pmb_reader_info']->cb),
);
$rows[] = array(
  t('Join date'),
  $reader->data['pmb_reader']['pmb_reader_info']->adhesion_date,
);
$rows[] = array(
  t('Expiration date'),
  $reader->data['pmb_reader']['pmb_reader_info']->expiration_date,
);

$template .= theme('table', array('header' => $header, 'rows' => $rows));
