<?php

/**
 * @file
 * PMB reader administration form.
 */

/**
 * Reader admin form.
 *
 * @ingroup forms
 * @see pmb_admin_form()
 */
function pmb_reader_admin_form() {
  $form = array();

  $form['reader'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache of readers'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 5,
  );
  $form['reader']['pmb_reader_session_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the reader sessions in the cache'),
    '#default_value' => pmb_variable_get('pmb_reader_session_duration'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('The duration of the reader sessions in the cache, in seconds.'),
    '#required' => TRUE,
  );
  $form['reader']['pmb_cache_duration_reader_loans'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the loans of readers in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_reader_loans'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('The duration of the loans of readers in the cache, in seconds'),
    '#required' => TRUE,
  );
  $form['reader']['pmb_cache_duration_reader_reservations'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the reservations of readers in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_reader_reservations'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('The duration of the reservations readers in the cache, in seconds'),
    '#required' => TRUE,
  );

  $form['user_interface']['pmb_tabs_reader'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Reader tabs to display'),
    '#options' => array(
      'summary' => t('Summary'),
      'cart' => t('Cart'),
      'reservation' => t('Reservations'),
      'loan' => t('Loans'),
      'reading_list' => t('Reading lists'),
      'suggestion' => t('Suggestions'),
    ),
    '#default_value' => pmb_variable_get('pmb_tabs_reader'),
    '#description' => t('Note: Default tab cannot be hidden.'),
  );
  $form['user_interface']['pmb_tabs_reader_default'] = array(
    '#type' => 'select',
    '#title' => t('Default Reader tab'),
    '#options' => array(
      'summary' => t('Summary'),
      'cart' => t('Cart'),
      'reservation' => t('Reservations'),
      'loan' => t('Loans'),
      'reading_list' => t('Reading lists'),
      'suggestion' => t('Suggestions'),
    ),
    '#default_value' => pmb_variable_get('pmb_tabs_reader_default'),
  );

  return $form;
}
