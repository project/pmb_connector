<?php

/**
 * @file
 * PMB reader account.
 */

function pmb_reader_information($user) {
  drupal_set_title(t('My reader account: Summary'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  drupal_set_breadcrumb($breadcrumb);
  return theme('pmb_reader_account', array(
    'reader' => $user,
    'parameters' => array(),
  ));
}

function pmb_reader_loans($user) {
  drupal_set_title(t('My reader account: Loans'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Loans'), 'reader/' . $user->uid . '/loan');
  drupal_set_breadcrumb($breadcrumb);

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $loans = $pmb_data->reader_get_loans($user->data['pmb_reader']['pmb_credentials']['login'], $user);

  return theme('pmb_reader_loans', array(
    'reader' => $user,
    'loans' => $loans,
    'parameters' => array(),
  ));
}
