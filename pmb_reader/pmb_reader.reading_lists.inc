<?php

/**
 * @file
 * PMB reader reading lists.
 */

function pmb_reader_view_reading_lists($user) {
  drupal_set_title(t('My reader account: Reading Lists'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Reading Lists'), 'reader/' . $user->uid . '/reading_list');
  drupal_set_breadcrumb($breadcrumb);

  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  $reading_lists = $pmb_data->reader_get_reading_lists();

  return theme('pmb_reader_reading_lists', array(
    'reader' => $user,
    'reading_lists' => $reading_lists,
    'parameters' => array(),
  ));
}

function pmb_reader_view_reading_lists_public($user) {
  drupal_set_title(t('My reader account: Public Reading Lists'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Public Reading Lists'), 'reader/' . $user->uid . '/reading_list_public');
  drupal_set_breadcrumb($breadcrumb);

  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  $reading_lists = $pmb_data->reader_get_reading_lists_public();

  return theme('pmb_reader_reading_lists_public', array(
    'reader' => $user,
    'reading_lists' => $reading_lists,
    'parameters' => array(),
  ));
}

function pmb_reader_view_reading_list($user, $list_id) {
  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;
  $reading_lists = $pmb_data->reader_get_reading_lists();

  $the_list = FALSE;
  foreach ($reading_lists as $alist) {
    if ($alist->reading_list_id == $list_id) {
      $the_list = $alist;
      break;
    }
  }
  if (!$the_list) {
    drupal_set_title(t('Reading list not found!'));
    drupal_set_message(t('Reading list not found!'), 'error');
    return FALSE;
  }

  drupal_set_title(t('My reader account: Reading List: !item', array('!item' => $the_list->reading_list_name)));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Reading Lists'), 'reader/' . $user->uid . '/reading_list');
  $breadcrumb[] = l($the_list->reading_list_name, 'reader/' . $user->uid . '/reading_list/' . $the_list->reading_list_id);
  drupal_set_breadcrumb($breadcrumb);

  $notices = $pmb_data->get_notices($the_list->reading_list_notice_ids);

  return drupal_get_form('pmb_reader_reading_list_form', $the_list, $notices);
}

function pmb_reader_view_reading_list_public($user, $list_id) {
  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;
  $reading_lists = $pmb_data->reader_get_reading_lists_public();

  $the_list = FALSE;
  foreach ($reading_lists as $alist) {
    if ($alist->reading_list_id == $list_id) {
      $the_list = $alist;
      break;
    }
  }
  if (!$the_list) {
    drupal_set_title(t('Reading list not found!'));
    drupal_set_message(t('Reading list not found!'), 'error');
    return FALSE;
  }

  drupal_set_title(t('My reader account: Public Reading List: !item', array('!item' => $the_list->reading_list_name)));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Reading Lists'), 'reader/' . $user->uid . '/reading_list_public');
  $breadcrumb[] = l($the_list->reading_list_name, 'reader/' . $user->uid . '/reading_list_public/' . $the_list->reading_list_id);
  drupal_set_breadcrumb($breadcrumb);

  $notices = $pmb_data->get_notices($the_list->reading_list_notice_ids);

  return theme('pmb_reader_reading_list_public', array(
    'reader' => $user,
    'reading_lists' => $the_list,
    'notices' => $notices,
    'parameters' => array(),
  ));
}

function pmb_reader_add_notices_to_reading_list($user, $list_id, $notice_ids) {
  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;
  $pmb_data->add_notices_to_reading_list($list_id, $notice_ids);
}


function pmb_reader_remove_notices_from_reading_list($user, $list_id, $notice_ids) {
  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;
  $pmb_data->remove_notices_to_reading_list($list_id, $notice_ids);
}

function pmb_reader_empty_reading_list($user, $list_id) {
  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;
  $pmb_data->empty_reading_list($list_id);
}
