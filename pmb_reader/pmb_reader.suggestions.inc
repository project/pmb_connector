<?php

/**
 * @file
 * PMB reader suggestions.
 */

function pmb_reader_suggestions($user) {
  drupal_set_title(t('My reader account: Suggestions'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Suggestions'), 'reader/' . $user->uid . '/suggestion');
  drupal_set_breadcrumb($breadcrumb);

  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  $suggestions = $pmb_data->reader_get_suggestions();

  return theme('pmb_reader_suggestions', array(
    'reader' => $user,
    'suggestions' => $suggestions,
    'parameters' => array(),
  ));
}

function pmb_reader_suggestion_edit($user, $suggestion_id) {
  drupal_set_title(t('View, add or edit a suggestion'));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('My reader account'), 'reader/' . $user->uid);
  $breadcrumb[] = l(t('Suggestions'), 'reader/' . $user->uid . '/suggestion');
  drupal_set_breadcrumb($breadcrumb);

  if (!(isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])))
    return FALSE;

  $suggestion = array();

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
  $pmb_data = new pmb_data();
  $pmb_data->set_user($user);
  $categ_and_sources = $pmb_data->reader_get_suggestion_sources_and_categories();

  if ($suggestion_id == 'add') {

  }
  elseif ($suggestion_id + 0) {
    $suggestions = $pmb_data->reader_get_suggestions();

    foreach ($suggestions as $asuggestion) {
      if ($asuggestion->sugg_id == $suggestion_id) {
        $suggestion = $asuggestion;
        break;
      }
    }

    if (!$suggestion) {
      drupal_set_title(t('Suggestion not found!'));
      drupal_set_message(t('Suggestion not found!'), 'error');
      return '';
    }
  }

  $result = drupal_get_form('pmb_reader_suggestion_edit_form', $suggestion, $categ_and_sources);

  return $result;
}
