<?php

/**
 * @file
 * PMB browse shelves.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_catalog_shelves() {
  global $user;

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $shelves = $pmb_data->get_shelves();

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Shelves'), 'catalog/shelf');
  drupal_set_breadcrumb($breadcrumb);

  return theme('pmb_catalog_shelves', array(
    'shelves' => $shelves,
    'parameters' => array(),
  ));
}

function pmb_catalog_shelf($shelf_id, $page = 1) {
  if ($shelf_id === NULL) {
    drupal_goto();
  }

  global $user;

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $shelves = $pmb_data->get_shelves();

  $shelf = array();
  foreach ($shelves as $ashelf) {
    if ($ashelf->id == $shelf_id) {
      $shelf = $ashelf;
      break;
    }
  }

  if (!$shelf) {
    drupal_set_title(t('Shelf not found!'));
    drupal_set_message(t('Shelf not found!'), 'error');
    return '';
  }

  drupal_set_title(t('Shelf: !item', array('!item' => $shelf->name)));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Shelves'), 'catalog/shelf');
  $breadcrumb[] = l($shelf->name, 'catalog/shelf/' . $shelf->id);
  drupal_set_breadcrumb($breadcrumb);

  $notice_ids = $pmb_data->get_shelf_notice_ids($shelf_id);
  if (!$notice_ids)
    $notice_ids = array();

  $notice_count = pmb_variable_get('pmb_noticeperpage_shelf');
  $notices_pages = array_chunk($notice_ids, $notice_count);
  $page = min($page, count($notices_pages));
  $page = max(1, $page);

  $notices = (count($notices_pages)) ?
    $pmb_data->get_notices($notices_pages[$page - 1]) :
    array();

  return theme('pmb_catalog_shelf', array(
    'shelf' => $shelf,
    'notices' => $notices,
    'parameters' => array(
      'page_number' => $page,
      'notices_per_pages' => $notice_count,
      'section_notice_count' => count($notice_ids),
  )));
}