<?php

/**
 * @file
 * PMB browse section.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_catalog_section($section_id, $page = 1) {
  global $user;

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $locations_and_sections = $pmb_data->get_locations_and_sections();

  $section = array();
  $location = array();
  foreach ($locations_and_sections as $alocation) {
    foreach ($alocation->sections as $asection) {
      if ($asection->section_id == $section_id) {
        $section = $asection;
        break;
      }
    }
    if ($section) {
      $location = $alocation;
      break;
    }
  }

  if (!$section || !$location) {
    if (!$location) {
      drupal_set_title(t('Location not found!'));
      drupal_set_message(t('Location not found!'), 'error');
    }
    if (!$section) {
      drupal_set_title(t('Section not found!'));
      drupal_set_message(t('Section not found!'), 'error');
    }
    return '';
  }

  drupal_set_title(t('Section: !item', array('!item' => $section->section_caption)));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Locations'), 'catalog/location');
// $breadcrumb[] = l($location->location->location_caption, 'catalog/location/' . $location->location->location_id);
  $breadcrumb[] = l(t('Section: !item', array('!item' => $section->section_caption)), 'catalog/section/' . $section->section_id);
  drupal_set_breadcrumb($breadcrumb);

  $notice_ids = $pmb_data->get_section_notice_ids($section_id);
  if (!$notice_ids)
    $notice_ids = array();

  $notice_count = pmb_variable_get('pmb_noticeperpage_section');
  $notices_pages = array_chunk($notice_ids, $notice_count);
  $page = min($page, count($notices_pages));
  $page = max(1, $page);

  $notices = (count($notices_pages)) ?
    $pmb_data->get_notices($notices_pages[$page - 1]) :
    array();

  return theme('pmb_catalog_section', array(
    'location' => $location,
    'section' => $section,
    'notices' => $notices,
    'parameters' => array(
      'page_number' => $page,
      'notices_per_pages' => $notice_count,
      'section_notice_count' => count($notice_ids),
  )));
}
