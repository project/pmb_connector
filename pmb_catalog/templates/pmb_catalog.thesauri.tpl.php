<?php

/**
 * @file
 * PMB browse thesauri template.
 */

$header = array(
  t('Thesaurus'),
);

$rows = array();
if (isset($thesauri) && is_array($thesauri) && count($thesauri)) {
  foreach ($thesauri as $thesaurus) {
    $rows[] = array(l($thesaurus->thesaurus_caption, 'catalog/category/' . $thesaurus->thesaurus_num_root_node));
  }
}
$template .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No thesaurus.')));
