<?php

/**
 * @file
 * PMB browse shelves template.
 */

$header = array(
  t('Shelf'),
  t('Comment'),
);

$rows = array();
if (isset($shelves) && is_array($shelves) && count($shelves)) {
  foreach ($shelves as $ashelf) {
    $rows[] = array(l($ashelf->name, 'catalog/shelf/' . $ashelf->id), $ashelf->comment);
  }
}

$template .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No shelf.')));
