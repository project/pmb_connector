<?php

/**
 * @file
 * PMB browse location template.
 */

$header = array(
  '',
  t('Section'),
);

$rows = array();
foreach ($location->sections as $asection) {
  $rows[] = array('<img src="http://tence.bibli.fr/opac/' . $asection->section_image . '"/>', l($asection->section_caption, 'catalog/section/' . $asection->section_id));
}

$template .= theme('table', array('header' => $header, 'rows' => $rows));
