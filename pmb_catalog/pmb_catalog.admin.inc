<?php

/**
 * @file
 * PMB catalog administration form.
 */

/**
 * Catalog admin form.
 *
 * @ingroup forms
 * @see pmb_admin_form()
 */
function pmb_catalog_admin_form() {
  $form = array();

  $form['browse'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display browse lists'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 7,
  );
  $form['browse']['pmb_noticeperpage_author'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per page / Authors'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_author'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('Number of record per page in the author pages'),
    '#required' => TRUE,
  );
  $form['browse']['pmb_noticeperpage_publisher'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per page / Publishers'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_publisher'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('Number of records per page in the publisher pages'),
    '#required' => TRUE,
  );
  $form['browse']['pmb_noticeperpage_collection'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per page / Collections'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_collection'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('Number of records per page in the collection pages'),
    '#required' => TRUE,
  );
  $form['browse']['pmb_noticeperpage_subcollection'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per page / Sub-Collections'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_subcollection'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('Number of records per page in the sub-collection pages'),
    '#required' => TRUE,
  );
  $form['browse']['pmb_noticeperpage_block_shelf'] = array(
    '#type' => 'textfield',
    '#title' => t('Records in shelf block'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_block_shelf'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t('Number of records displayed in a shelf block'),
    '#required' => TRUE,
  );

  $form['user_interface']['pmb_menu_display']['pmb_tabs_catalog'] = array(
    '#type' => 'checkboxes',
    '#title' => t('"Browse catalog" main menu'),
    '#options' => array(
      'location' => t('Locations'),
      'serial' => t('Serials'),
      'shelf' => t('Shelves'),
      'thesaurus' => t('Thesaurus'),
    ),
    '#default_value' => pmb_variable_get('pmb_tabs_catalog'),
    '#description' => t('Note: Default "Locations" tab cannot be hidden.'),
  );

  return $form;
}