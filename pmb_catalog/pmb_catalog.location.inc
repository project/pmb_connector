<?php

/**
 * @file
 * PMB browse location.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_catalog_location($location_id) {
  global $user;

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $locations_and_sections = $pmb_data->get_locations_and_sections();

  if (!isset($locations_and_sections[$location_id])) {
    drupal_set_title(t('Location not found!'));
    drupal_set_message(t('Location not found!'), 'error');
    return '';
  }
  $location = $locations_and_sections[$location_id];

  drupal_set_title(t('Location: !item', array('!item' => $location->location->location_caption)));
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Locations'), 'catalog/location');
  $breadcrumb[] = l($location->location->location_caption, 'catalog/location/' . $location_id);
  drupal_set_breadcrumb($breadcrumb);

  return theme('pmb_catalog_location', array(
    'location' => $location,
    'parameters' => array(),
  ));
}
