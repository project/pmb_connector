<?php

/**
 * @file
 * PMB browse locations.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_catalog_locations() {
  global $user;

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $locations_and_sections = $pmb_data->get_locations_and_sections();

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Locations'), 'catalog/location');
  drupal_set_breadcrumb($breadcrumb);

  return theme('pmb_catalog_locations', array(
    'locations_and_sections' => $locations_and_sections,
    'parameters' => array(),
  ));
}
