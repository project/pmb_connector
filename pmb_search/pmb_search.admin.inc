<?php

/**
 * @file
 * PMB search administration form.
 */

/**
 * Search admin form.
 *
 * @ingroup forms
 * @see pmb_admin_form()
 */
function pmb_search_admin_form() {
  $form = array();

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display search results'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 9,
  );
  $form['search']['pmb_noticeperpage_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Records per page / Search'),
    '#default_value' => pmb_variable_get('pmb_noticeperpage_search'),
    '#size' => 5,
    '#maxlength' => 10,
    '#description' => t("Number of records per page in the search results"),
    '#required' => TRUE,
  );

  $form['user_interface']['pmb_menu_display']['pmb_tabs_search'] = array(
    '#type' => 'checkboxes',
    '#title' => t('"Search catalog" main menu'),
    '#options' => array(
      'search/local' => t('Local search'),
      'search/external' => t('External search'),
      'search/advanced' => t('Advanced search'),
    ),
    '#default_value' => pmb_variable_get('pmb_tabs_search'),
    '#description' => t('Note: Default "Local search" tab cannot be hidden.'),
  );

  return $form;
}
