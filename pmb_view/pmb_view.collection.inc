<?php

/**
 * @file
 * PMB view collection.
 */

  require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_view_collection($collection_id, $page = 1) {
  global $user;

  if (!$collection_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $collection = $pmb_data->get_collection($collection_id);

  if (!$collection) {
    drupal_set_title(t('Collection not found!'));
    drupal_set_message(t('Collection not found!'), 'error');
    return '';
  }

  $title = '';
  if (isset($collection->information->collection_name))
    $title = $collection->information->collection_name;
  $title = trim($title);
  drupal_set_title(t('Collection: !item', array('!item' => $title)));

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Collection: !item', array('!item' => $title)), 'catalog/collection/' . $collection_id);
  drupal_set_breadcrumb($breadcrumb);

  $page += 0;

  $notice_count = pmb_variable_get('pmb_noticeperpage_collection');
  $notices_pages = array_chunk($collection->notice_ids, $notice_count);
  $page = min($page, count($notices_pages));
  $page = max(1, $page);

  $notices = (count($notices_pages)) ?
    $pmb_data->get_notices($notices_pages[$page - 1]) :
    array();

  $parent_publisher = $pmb_data->get_publisher($collection->information->collection_parent);

  return theme('pmb_view_collection', array(
    'collection' => $collection,
    'parent_publisher' => $parent_publisher,
    'parameters' => array(
      'notices' => $notices,
      'page_number' => $page,
      'notices_per_pages' => $notice_count,
  )));
}
