<?php

/**
 * @file
 * PMB info notice template.
 */

$notice_id = $notice['id'];
$notice_notice = $notice['notice'];
switch ($info) {
  case 'title' :
    $title = '';
    if (isset($notice_notice['f']['200'][0]['a'])) {
      $title = $notice_notice['f']['200'][0]['a'];
    }
    $template .= $title;
    break;
  case 'link' :
    switch ($notice_notice['header']['hl']) {
      case 1:
        $link = 'catalog/serial/' . $notice_id . '/';
        break;
      case 2:
        $link = 'catalog/record/' . $notice_id . '/';
        break;
      default:
      case 0:
      $link = 'catalog/record/' . $notice_id . '/';
        break;
    }
    $template .= url($link);
    break;
}
