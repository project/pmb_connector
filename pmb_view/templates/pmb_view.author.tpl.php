<?php

/**
 * @file
 * PMB view author template.
 */

$author_id = $author->information->author_id;
$author_author = $author->information;

$template .= '<br />';
$template .= '<div id="author_' . $author_id . '">';

$template .= '<h2>' . t('Information') . '</h2>';
$template .= '<div style="float: left;" id="author_' . $author_id . '_table">';

$header = array();
$rows = array();

if (check_plain($author_author->author_name . ($author_author->author_rejete ? ', ' . $author_author->author_rejete : '')))
  $rows[] = array(t('Name'), check_plain($author_author->author_name . ($author_author->author_rejete ? ', ' . $author_author->author_rejete : '')));

if (check_plain($author_author->author_date))
  $rows[] = array(t('Date'), $author_author->author_date);
if (check_plain($author_author->author_web))
  $rows[] = array(t('Web'), $author_author->author_web);
if (check_plain($author_author->author_lieu))
  $rows[] = array(t('Location'), $author_author->author_lieu . ' ' . $author_author->author_ville);
if (check_plain($author_author->author_comment))
  $rows[] = array(t('Comment'), $author_author->author_comment);

$template .= theme('table', array('header' => $header, 'rows' => $rows));

$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<h2>' . t('Records') .'</h2>';
$template .= '<div style="float: left;" id="author_' . $author_id . '_notices">';
if (isset($parameters['notices'])) {
  $header = array();
  $rows = array();
  foreach ($parameters['notices'] as $anotice) {
    $rows[] = array(theme('pmb_view_notice_display', array(
      'notice' => $anotice,
      'display_type' => 'medium_line',
      'parameters' => array(),
    )));
  }
  $template .= theme('table', array('header' => $header, 'rows' => $rows));
}
else {
  foreach ($author->notice_ids as $anotice) {
    $anotice += 0;
    $template .= l($anotice, 'catalog/record/' . $anotice . '/') . '<br />';
  }
}

$link_maker_function = create_function('$page_number', 'return "catalog/author/' . $author_id . '/" . $page_number;');

$template .= theme('pmb_pager', array(
  'current_page' => $parameters['page_number'],
  'page_count' => ceil(count($author->notice_ids) / $parameters['notices_per_pages']),
  'tags' => array(),
  'quantity' => 7,
  'link_generator_callback' => $link_maker_function,
));

$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<div>';
$template .= '<h2>' . t('Linked entities') . '</h2>';

$header = array();
$rows = array();
foreach ($author->information->author_links as $alink) {
  $link = '';
  switch ($alink->autlink_to) {
    case 1:
      $link = 'catalog/author/' . $alink->autlink_to_id;
      break;
    case 2:
      $link = 'catalog/category/' . $alink->autlink_to_id;
      break;
    case 3:
      $link = 'catalog/publisher/' . $alink->autlink_to_id;
      break;
    case 4:
      $link = 'catalog/collection/' . $alink->autlink_to_id;
      break;
    case 5:
      $link = 'catalog/subcollection/' . $alink->autlink_to_id;
      break;
    default:
      break;
  }
  $rows[] = array(l($alink->autlink_to_libelle, $link));
}
$template .= theme('table', array('header' => $header, 'rows' => $rows));

$template .= '</div>';
$template .= '</div>';
