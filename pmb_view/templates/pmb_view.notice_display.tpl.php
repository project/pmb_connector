<?php

/**
 * @file
 * PMB display notice template.
 */

$notice_id = $notice['id'];
$notice_notice = $notice['notice'];

switch ($display_type) {
  case 'title_author':
    $cover_url = '';

    if (isset($notice_notice['f']['896'][0]['a'])) {
      $cover_url = $notice_notice['f']['896'][0]['a'];
    }

    if ($cover_url) {
      $template .= '<div id="notice_' . $notice_id . '_cover" style="display:inline-block;">';
      $template .= '<img src="' . check_plain($cover_url) . '" style="max-width: 80px;">';
      $template .= '</div>';
    }

    $title = '';
    if (isset($notice_notice['f']['200'][0]['a'])) {
      $title = $notice_notice['f']['200'][0]['a'];
    }

    $authors = array();
    if (isset($notice_notice['f']['700'])) {
      foreach ($notice_notice['f']['700'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['701'])) {
      foreach ($notice_notice['f']['701'] as $afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['702'])) {
      foreach ($notice_notice['f']['702'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    switch ($notice_notice['header']['hl']) {
        case 1:
          $link = 'catalog/serial/' . $notice_id . '/';
          break;
        case 2:
          $link = 'catalog/record/' . $notice_id . '/';
          break;
        default:
        case 0:
        $link = 'catalog/record/' . $notice_id . '/';
          break;
      }

    $template .= '<div id="notice_' . $notice_id . '_info" style="display:inline-block;margin-left:5px;vertical-align:top;">';
    if ($title) {
      $template .= l($title, $link);
      $template .= '<br />';
    }
    if ($authors) {
      $template .= '[' . implode(', ', $authors) . ']';
      $template .= '<br />';
    }
    $template .= '</div>';
    break;

  case 'small_line':
    $cover_url = '';

    if (isset($notice_notice['f']['896'][0]['a'])) {
      $cover_url = $notice_notice['f']['896'][0]['a'];
    }

    if ($cover_url) {
      $template .= '<div id="notice_' . $notice_id . '_cover" style="display:inline-block;">';
      $template .= '<img src="' . check_plain($cover_url) . '" style="max-width: 80px;">';
      $template .= '</div>';
    }

    $title = '';
    if (isset($notice_notice['f']['200'][0]['a'])) {
      $title = $notice_notice['f']['200'][0]['a'];
    }

    $authors = array();
    if (isset($notice_notice['f']['700'])) {
      foreach ($notice_notice['f']['700'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['701'])) {
      foreach ($notice_notice['f']['701'] as $afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['702'])) {
      foreach ($notice_notice['f']['702'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    $publisher = array();
    if (isset($notice_notice['f']['210'])) {
      foreach ($notice_notice['f']['210'] as &$afield) {
        $display = '';
        if (isset($afield['c']) && $afield['c'] != '') {
          $publisher[] = l($afield['c'], 'catalog/publisher/' . $afield['id']);
        }
      }
    }

    switch ($notice_notice['header']['hl']) {
        case 1:
          $link = 'catalog/serial/' . $notice_id . '/';
          break;
        case 2:
          $link = 'catalog/record/' . $notice_id . '/';
          break;
        default:
        case 0:
        $link = 'catalog/record/' . $notice_id . '/';
          break;
      }

    $template .= '<div id="notice_' . $notice_id . '_info" style="display:inline-block;margin-left:5px;vertical-align:top;">';
    if ($title) {
      $template .= t('Title: !item', array('!item' => l($title, $link)));
      $template .= '<br />';
    }
    if ($authors) {
      $template .= t('Authors: !item', array('!item' => implode(', ', $authors)));
      $template .= '<br />';
    }
    if ($publisher) {
      $template .= t('Publisher: !item', array('!item' => implode(', ', $publisher)));
    }
    $template .= '</div>';
    break;

  case 'medium_line':
    $cover_url = '';

    if (isset($notice_notice['f']['896'][0]['a'])) {
      $cover_url = $notice_notice['f']['896'][0]['a'];
    }

    if ($cover_url) {
      $template .= '<div id="notice_' . $notice_id . '_cover" style="display:inline-block;">';
      $template .= '<img src="' . check_plain($cover_url) . '" style="max-width: 40px;">';
      $template .= '</div>';
    }

    $title = '';
    if (isset($notice_notice['f']['200'][0]['a'])) {
      $title = $notice_notice['f']['200'][0]['a'];
    }

    $authors = array();
    if (isset($notice_notice['f']['700'])) {
      foreach ($notice_notice['f']['700'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['701'])) {
      foreach ($notice_notice['f']['701'] as $afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    if (isset($notice_notice['f']['702'])) {
      foreach ($notice_notice['f']['702'] as &$afield) {
        $display = '';
        $display .= isset($afield['a']) ? $afield['a'] . ' ' : '';
        $display .= isset($afield['b']) ? $afield['b'] . ' ' : '';
        $display = trim($display);
        $authors[] = l($display, 'catalog/author/' . $afield['id'] . '/');
      }
    }

    $publisher = array();
    if (isset($notice_notice['f']['210'])) {
      foreach ($notice_notice['f']['210'] as &$afield) {
        $display = '';
        if (isset($afield['c']) && $afield['c'] != '') {
          $publisher[] = l($afield['c'], 'catalog/publisher/' . $afield['id']);
        }
      }
    }

    switch ($notice_notice['header']['hl']) {
        case 1:
          $link = 'catalog/serial/' . $notice_id . '/';
          break;
        case 2:
          $link = 'catalog/record/' . $notice_id . '/';
          break;
        default:
        case 0:
        $link = 'catalog/record/' . $notice_id . '/';
          break;
      }

    $template .= '<div id="notice_' . $notice_id . '_info" style="display:inline-block;margin-left:5px;vertical-align:top;">';
    if ($title) {
      $template .= t('Title: !item', array('!item' => l($title, $link)));
      $template .= '<br />';
    }
    if ($authors) {
      $template .= t('Authors: !item', array('!item' => implode(', ', $authors)));
      $template .= '<br />';
    }
    if ($publisher) {
      $template .= t('Publisher: !item', array('!item' => implode(', ', $publisher)));
    }
    $template .= '</div>';
    break;
}
