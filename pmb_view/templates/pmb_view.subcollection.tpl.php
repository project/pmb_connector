<?php

/**
 * @file
 * PMB view subcollection template.
 */

$subcollection_id = $subcollection->information->sous_collection_id;
$subcollection_subcollection = $subcollection->information;

$template .= '<br />';
$template .= '<div id="subcollection_' . $subcollection_id . '">';

$template .= '<h2>' . t('Information') . '</h2>';
$template .= '<div style="float: left;" id="subcollection_' . $subcollection_id . '_table">';

$header = array();
$rows = array();

$rows[] = array(t('Name'), $subcollection_subcollection->sous_collection_name);
if ($parent_collection) {
  $rows[] = array(t('Collection'), l($parent_collection->information->collection_name, 'catalog/collection/' . $parent_collection->information->collection_id));
}
if ($subcollection_subcollection->sous_collection_issn) {
  $rows[] = array(t('ISSN'), $subcollection_subcollection->sous_collection_issn);
}
if ($subcollection_subcollection->sous_collection_web) {
  $rows[] = array(t('Web'), $subcollection_subcollection->sous_collection_web);
}

$template .= theme('table', array('header' => $header, 'rows' => $rows));

$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<h2>' . t('Records') . '</h2>';
$template .= '<div style="float: left;" id="subcollection_' . $subcollection_id . '_notices">';
if (isset($parameters['notices'])) {
  $header = array();
  $rows = array();
  foreach ($parameters['notices'] as $anotice) {
    $rows[] = array(theme('pmb_view_notice_display', array(
      'notice' => $anotice,
      'display_type' => 'medium_line',
      'parameters' => array(),
    )));
  }
  $template .= theme('table', array('header' => $header, 'rows' => $rows));
}
else {
  foreach ($subcollection->notice_ids as $anotice) {
    $anotice += 0;
    $template .= l($anotice, 'catalog/record/' . $anotice . '/') .'<br />';
  }
}

$link_maker_function = create_function('$page_number', 'return "catalog/subcollection/' . $subcollection_id . '/" . $page_number;');

$template .= theme('pmb_pager', array(
  'current_page' => $parameters['page_number'],
  'page_count' => ceil(count($subcollection->notice_ids) / $parameters['notices_per_pages']),
  'tags' => array(),
  'quantity' => 7,
  'link_generator_callback' => $link_maker_function,
));

$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<div>';
$template .= '<h2>' . t('Linked entities') . '</h2>';
$header = array();
$rows = array();
foreach ($subcollection->information->sous_collection_links as $alink) {
  $link = '';
  switch ($alink->autlink_to) {
    case 1:
      $link = 'catalog/author/' . $alink->autlink_to_id;
      break;
    case 2:
      $link = 'catalog/category/' . $alink->autlink_to_id;
      break;
    case 3:
      $link = 'catalog/publisher/' . $alink->autlink_to_id;
      break;
    case 4:
      $link = 'catalog/collection/' . $alink->autlink_to_id;
      break;
    case 5:
      $link = 'catalog/subcollection/' . $alink->autlink_to_id;
      break;
    default:
      break;
  }
  $rows[] = array(l($alink->autlink_to_libelle, $link));
}
$template .= theme('table', array('header' => $header, 'rows' => $rows));

$template .= '</div>';
$template .= '</div>';
