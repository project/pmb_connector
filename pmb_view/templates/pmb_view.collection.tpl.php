<?php

/**
 * @file
 * PMB view collection template.
 */

$collection_id = $collection->information->collection_id;
$collection_collection = $collection->information;

$template .= '<br />';
$template .= '<div id="collection_' . $collection_id . '">';

$template .= '<h2>' . t('Information') . '</h2>';
$template .= '<div style="float: left;" id="collection_' . $collection_id . '_table">';

$header = array();
$rows = array();

$rows[] = array(t('Name'), $collection_collection->collection_name);
if ($parent_publisher) {
  $rows[] = array(t('Publisher'), l($parent_publisher->information->publisher_name, 'catalog/publisher/' . $parent_publisher->information->publisher_id));
}
if ($collection_collection->collection_issn) {
  $rows[] = array(t('ISSN'), $collection_collection->collection_issn);
}
if ($collection_collection->collection_web) {
  $rows[] = array(t('Web'), $collection_collection->collection_web);
}

$template .= theme('table', array('header' => $header, 'rows' => $rows));
$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<h2>' . t('Records') . '</h2>';
$template .= '<div style="float: left;" id="collection_' . $collection_id . '_notices">';
if (isset($parameters['notices'])) {
  $header = array();
  $rows = array();
  foreach ($parameters['notices'] as $anotice) {
    $rows[] = array(theme('pmb_view_notice_display', array(
      'notice' => $anotice,
      'display_type' => 'medium_line',
      'parameters' => array(),
    )));
  }
  $template .= theme('table', array('header' => $header, 'rows' => $rows));
}
else {
  foreach ($collection->notice_ids as $anotice) {
    $anotice += 0;
    $template .= l($anotice, 'catalog/record/' . $anotice . '/') . '<br />';
  }
}

$link_maker_function = create_function('$page_number', 'return "catalog/collection/' . $collection_id . '/" . $page_number;');

$template .= theme('pmb_pager', array(
  'current_page' => $parameters['page_number'],
  'page_count' => ceil(count($collection->notice_ids) / $parameters['notices_per_pages']),
  'tags' => array(),
  'quantity' => 7,
  'link_generator_callback' => $link_maker_function,
));

$template .= '</div>';

$template .= '<br style="clear: both"/>';
$template .= '<div>';
$template .= '<h2>' . t('Linked entities') . '</h2>';

$header = array();
$rows = array();
foreach ($collection->information->collection_links as $alink) {
  $link = '';
  switch ($alink->autlink_to) {
    case 1:
      $link = 'catalog/author/' . $alink->autlink_to_id;
      break;
    case 2:
      $link = 'catalog/category/' . $alink->autlink_to_id;
      break;
    case 3:
      $link = 'catalog/publisher/' . $alink->autlink_to_id;
      break;
    case 4:
      $link = 'catalog/collection/' . $alink->autlink_to_id;
      break;
    case 5:
      $link = 'catalog/subcollection/' . $alink->autlink_to_id;
      break;
    default:
      break;
  }
  $rows[] = array(l($alink->autlink_to_libelle, $link));
}
$template .= theme('table', array('header' => $header, 'rows' => $rows));

$template .= '</div>';
$template .= '</div>';
