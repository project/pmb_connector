<?php

/**
 * @file
 * PMB main module.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');
include_once(drupal_get_path('module', 'pmb_view') . '/pmb_view.templates.inc');

/**
 * Implements hook_permission().
 */
function pmb_view_permission() {
  return array(
    'PMB view notice' => array(
      'title' => t('View notices'),
    ),
    'PMB view author' => array(
      'title' => t('View authors'),
    ),
    'PMB view collection' => array(
      'title' => t('View collections'),
    ),
    'PMB view publisher' => array(
      'title' => t('View publishers'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function pmb_view_menu() {
  $items = array();

  $items['catalog/record/%'] = array(
    'title' => 'View Record',
    'description' => 'View a record',
    'page callback' => 'pmb_view_notice',
    'page arguments' => array(2),
    'access arguments' => array('PMB view notice'),
    'file' => 'pmb_view.notice.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/external_record/%'] = array(
    'title' => 'View External Record',
    'description' => 'View an external record',
    'page callback' => 'pmb_view_notice_external',
    'page arguments' => array(2),
    'access arguments' => array('PMB view notice'),
    'file' => 'pmb_view.notice.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/serial/%'] = array(
    'title' => 'View Serial',
    'description' => 'View a serial',
    'page callback' => 'pmb_view_serial',
    'page arguments' => array(2),
    'access arguments' => array('PMB view notice'),
    'file' => 'pmb_view.notice.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/serial/%/%'] = array(
    'title' => 'View Serial',
    'description' => 'View a serial',
    'page callback' => 'pmb_view_serial',
    'page arguments' => array(2, 3),
    'access arguments' => array('PMB view notice'),
    'file' => 'pmb_view.notice.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/bulletin/%'] = array(
    'title' => 'View Issue',
    'description' => 'View an issue',
    'page callback' => 'pmb_view_bulletin',
    'page arguments' => array(2),
    'access arguments' => array('PMB view notice'),
    'file' => 'pmb_view.notice.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/author/%'] = array(
    'title' => 'View Author',
    'description' => 'View an author',
    'page callback' => 'pmb_view_author',
    'page arguments' => array(2),
    'access arguments' => array('PMB view author'),
    'file' => 'pmb_view.author.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/author/%/%'] = array(
    'title' => 'View Author',
    'description' => 'View an author',
    'page callback' => 'pmb_view_author',
    'page arguments' => array(2, 3),
    'access arguments' => array('PMB view author'),
    'file' => 'pmb_view.author.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/publisher/%'] = array(
    'title' => 'View Publisher',
    'description' => 'View a Publisher',
    'page callback' => 'pmb_view_publisher',
    'page arguments' => array(2),
    'access arguments' => array('PMB view publisher'),
    'file' => 'pmb_view.publisher.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/publisher/%/%'] = array(
    'title' => 'View Publisher',
    'description' => 'View a Publisher',
    'page callback' => 'pmb_view_publisher',
    'page arguments' => array(2, 3),
    'access arguments' => array('PMB view publisher'),
    'file' => 'pmb_view.publisher.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/collection/%'] = array(
    'title' => 'View Collection',
    'description' => 'View a Collection',
    'page callback' => 'pmb_view_collection',
    'page arguments' => array(2),
    'access arguments' => array('PMB view collection'),
    'file' => 'pmb_view.collection.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/collection/%/%'] = array(
    'title' => 'View Collection',
    'description' => 'View a Collection',
    'page callback' => 'pmb_view_collection',
    'page arguments' => array(2, 3),
    'access arguments' => array('PMB view collection'),
    'file' => 'pmb_view.collection.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/subcollection/%'] = array(
    'title' => 'View Subcollection',
    'description' => 'View a Subcollection',
    'page callback' => 'pmb_view_subcollection',
    'page arguments' => array(2),
    'access arguments' => array('PMB view collection'),
    'file' => 'pmb_view.subcollection.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  $items['catalog/subcollection/%/%'] = array(
    'title' => 'View Subcollection',
    'description' => 'View a Subcollection',
    'page callback' => 'pmb_view_subcollection',
    'page arguments' => array(2, 3),
    'access arguments' => array('PMB view collection'),
    'file' => 'pmb_view.subcollection.inc',
    'type' => MENU_NORMAL_ITEM,
    'hidden' => 1,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function pmb_view_theme() {
  return array(
    'pmb_view_notice' => array(
      'variables' => array(
        'notice' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_notice_info' => array(
      'variables' => array(
        'notice' => NULL,
        'info' => NULL,
      ),
    ),
    'pmb_view_notice_external' => array(
      'variables' => array(
        'notice' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_serial' => array(
      'variables' => array(
        'notice' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_bulletin' => array(
      'variables' => array(
        'bulletin' => NULL,
        'serial' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_notice_display' => array(
      'variables' => array(
        'notice' => NULL,
        'display_type' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_notice_external_display' => array(
      'variables' => array(
        'notice' => NULL,
        'display_type' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_author' => array(
      'variables' => array(
        'author' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_publisher' => array(
      'variables' => array(
        'publisher' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_collection' => array(
      'variables' => array(
        'collection' => NULL,
        'parent_publisher' => NULL,
        'parameters' => NULL,
      ),
    ),
    'pmb_view_subcollection' => array(
      'variables' => array(
        'subcollection' => NULL,
        'parent_collection' => NULL,
        'parameters' => NULL,
      ),
    ),
  );
}
