<?php

/**
 * @file
 * PMB view publisher.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_view_publisher($publisher_id, $page = 1) {
  global $user;

  if (!$publisher_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $publisher = $pmb_data->get_publisher($publisher_id);

  if (!$publisher) {
    drupal_set_title(t('Publisher not found!'));
    drupal_set_message(t('Publisher not found!'), 'error');
    return '';
  }

  $title = '';
  if (isset($publisher->information->publisher_name))
    $title = $publisher->information->publisher_name;
  $title = trim($title);
  drupal_set_title(t('Publisher: !item', array('!item' => $title)));

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Publisher: !item', array('!item' => $title)), 'catalog/publisher/' . $publisher_id);
  drupal_set_breadcrumb($breadcrumb);

  $page += 0;

  $notice_count = pmb_variable_get('pmb_noticeperpage_publisher');
  $notices_pages = array_chunk($publisher->notice_ids, $notice_count);
  $page = min($page, count($notices_pages));
  $page = max(1, $page);

  $notices = (count($notices_pages)) ?
    $pmb_data->get_notices($notices_pages[$page - 1]) :
    array();

  return theme('pmb_view_publisher', array(
    'publisher' => $publisher,
    'parameters' => array(
      'notices' => $notices,
      'page_number' => $page,
      'notices_per_pages' => $notice_count,
  )));
}
