<?php

/**
 * @file
 * PMB view subcollection.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_view_subcollection($subcollection_id, $page = 1) {
  global $user;

  if (!$subcollection_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $subcollection = $pmb_data->get_subcollection($subcollection_id);

  if (!$subcollection) {
    drupal_set_title(t('Subcollection not found!'));
    drupal_set_message(t('Subcollection not found!'), 'error');
    return '';
  }

  $title = '';
  if (isset($subcollection->information->sous_collection_name))
    $title = $subcollection->information->sous_collection_name;
  $title = trim($title);
  drupal_set_title(t('Subcollection: !item', array('!item' => $title)));

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Subcollection: !item', array('!item' => $title)), 'catalog/subcollection/' . $subcollection_id);
  drupal_set_breadcrumb($breadcrumb);

  $page += 0;

  $notice_count = pmb_variable_get('pmb_noticeperpage_subcollection');
  $notices_pages = array_chunk($subcollection->notice_ids, $notice_count);
  $page = min($page, count($notices_pages));
  $page = max(1, $page);

  $notices = (count($notices_pages)) ?
    $pmb_data->get_notices($notices_pages[$page - 1]) :
    array();

  $parent_collection = $pmb_data->get_collection($subcollection->information->sous_collection_parent);

  return theme('pmb_view_subcollection', array(
    'subcollection' => $subcollection,
    'parent_collection' => $parent_collection,
    'parameters' => array(
      'notices' => $notices,
      'page_number' => $page,
      'notices_per_pages' => $notice_count,
  )));
}
