<?php

/**
 * @file
 * PMB view notices.
 */

require_once(drupal_get_path('module', 'pmb') . '/pmb.class_data.inc');

function pmb_view_notice($notice_id) {
  global $user;

  if (!$notice_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $notice = $pmb_data->get_notice($notice_id);

  if (!$notice || $notice['notice'] == 0) {
    drupal_set_title(t('Record not found!'));
    drupal_set_message(t('Record not found!'), 'error');
    return '';
  }

  if ($notice['notice']['header']['rs'] == 'n'
      && $notice['notice']['header']['dt'] == 'a'
      && $notice['notice']['header']['bl'] == 's'
      && $notice['notice']['header']['hl'] == '2'
    ) {
    //C'est un bulletin
    $bulletin_id = $pmb_data->find_notice_bulletin($notice_id);
    if ($bulletin_id)
      drupal_goto('catalog/bulletin/' . $bulletin_id);
  }

  $can_reserve_notice = FALSE;
  if (module_exists('pmb_reader')) {
    if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])) {
      $can_reserve_notice = $pmb_data->reader_can_reserve_notice($user->data['pmb_reader']['pmb_credentials']['login'], $user, $notice_id, 0);
    }
  }

  $title = '';
  if (isset($notice['notice']['f']['200'][0]['a']))
    $title = $notice['notice']['f']['200'][0]['a'];
  drupal_set_title(t('Record: !item', array('!item' => $title)));

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Record: !item', array('!item' => $title)), 'catalog/record/' . $notice_id);
  drupal_set_breadcrumb($breadcrumb);

  $no_items = FALSE;
  if (empty($notice['items'])) {
    $no_items = TRUE;
  }
  elseif ($notice['notice']['header']['rs'] == 'n'
      && $notice['notice']['header']['dt'] == 'a'
      && $notice['notice']['header']['bl'] == 'a'
      && $notice['notice']['header']['hl'] == '2'
    ) {
    // Item is an article.
    $no_items = TRUE;
  }

  $can_add_to_cart = FALSE;
  if (module_exists('pmb_reader')) {
    if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])) {
      $can_add_to_cart = TRUE;
    }
  }

  switch ($notice['notice']['header']['hl']) {
    case 1:
      return theme('pmb_view_serial', array(
        'notice' => $notice,
        'parameters' => array(
          'can_reserve' => $can_reserve_notice,
          'no_items' => $no_items,
      )));
      break;
    case 2:
      return theme('pmb_view_notice', array(
        'notice' => $notice,
        'parameters' => array(
          'can_reserve' => $can_reserve_notice,
          'no_items' => $no_items,
          'can_add_to_cart' => $can_add_to_cart,
      )));
      break;
    case 0:
    default:
      return theme('pmb_view_notice',  array(
        'notice' => $notice,
        'parameters' => array(
          'can_reserve' => $can_reserve_notice,
          'no_items' => $no_items,
          'can_add_to_cart' => $can_add_to_cart,
      )));
      break;
  }
}

function pmb_view_notice_external($notice_id) {
  global $user;

  if (!$notice_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();
  if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials']))
    $pmb_data->set_user($user);

  $notice = $pmb_data->get_notice_external($notice_id);

  if (!$notice || $notice['notice'] == 0) {
    drupal_set_title(t('Record not found!'));
    drupal_set_message(t('Record not found!'), 'error');
    return '';
  }

  $title = '';
  if (isset($notice['notice']['f']['200'][0]['a']))
    $title = $notice['notice']['f']['200'][0]['a'];
  if (isset($notice['notice']['f']['801'][0][9]))
    $source = $notice['notice']['f']['801'][0][9];
  drupal_set_title($source . ' : ' . $title);

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('External Record: !item', array('!item' => $title)), 'catalog/external_record/' . $notice_id);
  drupal_set_breadcrumb($breadcrumb);

  return theme('pmb_view_notice_external', array(
    'notice' => $notice,
    'parameters' => array(),
  ));
}

function pmb_view_serial($notice_id, $page = 1) {
  if (!$notice_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();

  $notice = $pmb_data->get_notice($notice_id);

  if (!$notice || $notice['notice'] == 0) {
    drupal_set_title(t('Serial not found!'));
    drupal_set_message(t('Serial not found!'), 'error');
    return '';
  }

  $title = '';
  if (isset($notice['notice']['f']['200'][0]['a']))
    $title = $notice['notice']['f']['200'][0]['a'];
  drupal_set_title(t('Serial: !item', array('!item' => $title)));

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Serials'), 'catalog/serial');
  $breadcrumb[] = l($title, 'catalog/serial/' . $notice_id);
  drupal_set_breadcrumb($breadcrumb);

  $page += 0;

  $bulletins_count = pmb_variable_get('pmb_bulletinsperpage_serials');
  $bulletins_pages = array_chunk($notice['bulletins'], $bulletins_count);
  $page = min($page, count($bulletins_pages));
  $page = max(1, $page);

  $bulletins = (count($bulletins_pages)) ?
    $bulletins_pages[$page - 1] :
    array();

  return theme('pmb_view_serial', array(
    'notice' => $notice,
    'parameters' => array(
      'bulletins' => $bulletins,
      'page_number' => $page,
      'bulletins_per_page' => $bulletins_count,
  )));
}

function pmb_view_bulletin($bulletin_id) {
  global $user;

  if (!$bulletin_id) {
    return t('ERROR!');
  }

  $pmb_data = new pmb_data();

  $bulletin = $pmb_data->get_bulletin($bulletin_id);

  if (!$bulletin) {
    drupal_set_title(t('Issue not found!'));
    drupal_set_message(t('Issue not found!'), 'error');
    return '';
  }
  $serial = $pmb_data->get_notice($bulletin['bulletin']->serial_id);

  $serial_title = (isset($serial['notice']['f']['200'][0]['a'])) ?
      $serial['notice']['f']['200'][0]['a'] :
      '';

  if ($bulletin['analysis']) {
    $analysis = $pmb_data->get_notices($bulletin['analysis']);
  }
  else {
    $analysis = array();
  }

  $can_reserve_notice = FALSE;
  if (module_exists('pmb_reader')) {
    if (isset($user->uid) && isset($user->data['pmb_reader']['pmb_credentials'])) {
      $can_reserve_notice = $pmb_data->reader_can_reserve_notice($user->data['pmb_reader']['pmb_credentials']['login'], $user, 0, $bulletin_id);
    }
  }

  $no_items = FALSE;
  if (empty($bulletin['items'])) {
    $no_items = TRUE;
  }

  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('Catalog'), 'catalog');
  $breadcrumb[] = l(t('Serials'), 'catalog/serial');
  $breadcrumb[] = l($serial_title, 'catalog/serial/' . $serial['id']);
  $breadcrumb[] = l(t('Bulletin: !item (!number)', array('!item' => $bulletin['bulletin']->bulletin_title, '!number' => $bulletin['bulletin']->bulletin_numero)), 'catalog/bulletin/' . $bulletin['bulletin']->bulletin_id);
  drupal_set_breadcrumb($breadcrumb);

  return theme('pmb_view_bulletin', array(
    'bulletin' => $bulletin,
    'serial' => $serial,
    'parameters' => array(
      'can_reserve' => $can_reserve_notice,
      'no_items' => $no_items,
      'analysis' => $analysis,
  )));
}
