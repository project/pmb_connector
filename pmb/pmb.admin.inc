<?php

/**
 * @file
 * PMB administration form.
 */

/**
 * Base PMB admin form.
 *
 * @ingroup forms
 * @see pmb_admin_form_submit()
 * @see system_settings_form()
 */
function pmb_admin_form() {
  $form = array();

  $form['tab'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'connector',
  );

  $form['tab']['connector'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link to PMB'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 1,
  );
  $form['tab']['connector']['pmb_link_type'] = array(
    '#type' => 'select',
    '#title' => t('PMB link type'),
    '#default_value' => pmb_variable_get('pmb_link_type'),
    '#description' => t("The type of the link to PMB"),
    '#options' => array(
      'json-rpc' => t('JSON-RPC'),
    ),
    '#required' => TRUE,
  );
  $form['tab']['connector']['pmb_link_serverurl'] = array(
    '#type' => 'textfield',
    '#title' => t('PMB service URL'),
    '#default_value' => pmb_variable_get('pmb_link_serverurl'),
    '#size' => 50,
    '#maxlength' => 256,
    '#description' => t("The url of the PMB service"),
    '#required' => TRUE,
  );
  $form['tab']['connector']['pmb_link_serveruser'] = array(
    '#type' => 'textfield',
    '#title' => t('PMB service login'),
    '#default_value' => pmb_variable_get('pmb_link_serveruser'),
    '#size' => 50,
    '#maxlength' => 256,
    '#description' => t('The login of the PMB service. Let empty in case of an anonymous use.'),
    '#required' => FALSE,
  );
  $form['tab']['connector']['pmb_link_serverpassword'] = array(
    '#type' => 'textfield',
    '#title' => t('The password of the PMB service'),
    '#default_value' => pmb_variable_get('pmb_link_serverpassword'),
    '#size' => 50,
    '#maxlength' => 256,
    '#description' => t('PMB service password. Let empty in case of an anonymous use.'),
    '#required' => FALSE,
  );

  $form['tab']['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache of records'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 3,
  );
  $form['tab']['cache']['pmb_user_cache_profile'] = array(
    '#type' => 'radios',
    '#title' => t('Cache profile'),
    '#default_value' => pmb_variable_get('pmb_user_cache_profile'),
    '#description' => t("The profile of the cache. Determine how to cache the notices. This will have an impact on performances."),
    '#required' => TRUE,
    '#options' => array(
      'cache_anonymous_all' => t('Anonymous and readers share the same cache.'),
      'cache_anonymous_empr_all' => t('Anonymous readers have their own cache. All readers share the same cache.'),
      'cache_anonymous_empr_separate' => t("Anonymous readers have their own cache. Each reader has its own cache."),
    ),
  );
  $form['tab']['cache']['pmb_cache_duration_notice'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the notices in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_notice'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the notices in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_author'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the authors in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_author'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the authors in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_publisher'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the publishers in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_publisher'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the publishers in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_collection'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the collections in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_collection'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the collections in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_subcollection'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the subcollections in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_subcollection'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the subcollections in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_locations_and_sections'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the locations and sections in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_locations_and_sections'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the locations and sections in the cache, in seconds"),
    '#required' => TRUE,
  );
  $form['tab']['cache']['pmb_cache_duration_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration of the searches in the cache'),
    '#default_value' => pmb_variable_get('pmb_cache_duration_search'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("The duration of the searches in the cache, in seconds"),
    '#required' => TRUE,
  );

  if (module_exists('pmb_search')
      || module_exists('pmb_catalog')
      || module_exists('pmb_reader')
    ) {
    $form['tab']['user_interface'] = array(
      '#type' => 'fieldset',
      '#title' => t('User interface'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 6,
    );
    $form['tab']['user_interface']['pmb_tabs_info'] = array(
      '#type' => 'item',
      '#title' => t('Tabs to display in main menu'),
    );
    // Items are filled by each module hook_pmb_admin_form_alter().
    $form['tab']['user_interface']['pmb_menu_display'] = array(
      '#type' => 'fieldset',
      '#description' => t('Uncheck checkboxes to hide a tab. Main menus can be disabled in <a href="!link">Administer > Structure > Menus > Main menu</a>.', array(
        '!link' => url('admin/structure/menu/manage/main-menu'),
      )),
    );
  }
  if (module_exists('pmb_search') && module_exists('pmb_catalog')) {
    $form['tab']['user_interface']['pmb_menu_default'] = array(
      '#type' => 'radios',
      '#title' => t('Default page used for main menu "www.example.com/catalog"'),
      '#options' => array(
        'search' => t('Search the catalog'),
        'browse' => t('Browse the catalog'),
      ),
      '#default_value' => pmb_variable_get('pmb_menu_default'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  $form['actions']['pmb_default_values'] = array(
    '#type'     => 'submit',
    '#value'    => t('Reset to defaults'),
    '#submit'   => array('_pmb_default_values'),
  );

  return $form;
}

/**
 * This submit function is needed only because it's impossible with a normal
 * hook_menu() to set a child as a main menu with its parent ('catalog/search'
 * or 'catalog/browse' on the same level than 'catalog').
 *
 * @see pmb_admin_form()
 */
function pmb_admin_form_submit($form, &$form_state) {
  // Check if menu options have changed in order to rebuild menus after saving
  // settings.
  $previous = &$form_state['complete form']['tab']['user_interface'];
  $values = &$form_state['values'];

  // Currently, default tab can't be hidden. You can use tabtamer module to do
  // that. In that case, all tabs should be checked here.
  if (isset($values['pmb_tabs_search']['search/local'])) {
    $values['pmb_tabs_search']['search/local'] = 'search/local';
  }
  if (isset($values['pmb_tabs_catalog']['locations'])) {
    $values['pmb_tabs_catalog']['locations'] = 'locations';
  }
  // Default tab can't be hidden.
  if (isset($values['pmb_tabs_reader_default'])) {
    $values['pmb_tabs_reader'][$values['pmb_tabs_reader_default']] = $values['pmb_tabs_reader_default'];
  }

  $menu_changed = (($previous['pmb_menu_default']['#default_value'] != $values['pmb_menu_default'])
      || (isset($values['pmb_tabs_search']) && ($previous['pmb_menu_display']['pmb_tabs_search']['#default_value'] !== $values['pmb_tabs_search']))
      || (isset($values['pmb_tabs_catalog']) && ($previous['pmb_menu_display']['pmb_tabs_catalog']['#default_value'] !== $values['pmb_tabs_catalog']))
      || (isset($values['pmb_tabs_reader']) && ($previous['pmb_tabs_reader']['#default_value'] !== $values['pmb_tabs_reader']))
      || (isset($values['pmb_tabs_reader_default']) && ($previous['pmb_tabs_reader_default']['#default_value'] !== $values['pmb_tabs_reader_default']))
    ) ?
    TRUE :
    FALSE;

  system_settings_form_submit($form, $form_state);

  if ($menu_changed) {
    menu_rebuild();
  }
}

/**
 * Restore recommended default values.
 *
 * @see pmb_admin_form()
 */
function _pmb_default_values($form, &$form_state) {
  foreach (pmb_default_variables() as $key => $value) {
    variable_set($key, $value);
  }
  unset($form_state['values']);
  unset($form_state['storage']);

  menu_rebuild();

  drupal_set_message(t('Options have been reset to default.'));
}
