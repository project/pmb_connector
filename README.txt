
                                PMB Connector
                                =============


-- SUMMARY --
  ---------

PMB Connector allows to link PMB/PhpMyBibli, a free (except easy access to old
and new code) library management system (LMS/ILS), to Drupal through a
webservice (currently JSON-RPC).

With this module, users can browse and search inside the catalog of the library
from Drupal. Readers can access to their loans, reservations and reading lists
from the CMS.

Current release of PMB/PhpMyBibli can be downloaded here at http://www.pmbservices.fr/nouveau_site/telecharger_inter.html.


-- INSTALLATION --
  --------------

To install, see documentation on http://doc.sigb.net/pmbdrupal.


-- LICENCE --
  ---------

Initial release of this module (pmbdrupal 0.9) is licensed under the GPL
compatible CeCILL licence. More details can be found on http://www.cecill.info
or in the file Licence_CeCILL_V2-en.txt.

Releases committed on http://drupal.org/project/pmb are published with a double
licence: CeCILL and GPL.


In consideration of access to the source code and the rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors only have limited liability.

In this respect, the risks associated with loading, using, modifying
and/or developing or reproducing the software by the user are brought to
the user's attention, given its Free Software status, which may make it
complicated to use, with the result that its use is reserved for
developers and experienced professionals having in-depth computer
knowledge. Users are therefore encouraged to load and test the
suitability of the software as regards their requirements in conditions
enabling the security of their systems and/or data to be ensured and,
more generally, to use and operate it in the same conditions of
security. This Agreement may be freely reproduced and published,
provided it is not altered, and that no provisions are either added or
removed herefrom.


-- WARNING --
  ---------

Use it at your own risk.
It's always recommended to backup your database so you can roll back if needed.


-- COPYRIGHT --
  ---------

Copyright © 2011 PMB Services <pmb@sigb.net> and contributors (see www.sigb.net)
  (first public release pmbdrupal 0.9)
Copyright © 2007 sergio <jsonrpcphp@inservibile.org>
  (JSON-RPC portion)
Copyright © 2011 Daniel Berthereau <daniel.drupal@berthereau.net>
  (debugging and enhancements of pmbdrupal 0.9 [1.0-ALPHA])
  (conversion for Drupal 7)


-- CONTACT --
  ---------

Daniel Berthereau (Daniel_KM) => http://drupal.org/user/428555
